require_relative "./counter"

class Counters
    def initialize(counters_quantity)
        @values = build_counters(counters_quantity)
    end

    def to_a
        @values.map do |value|
            value.to_i
        end 
    end

    def increase(counter_position)
        adapted_position = counter_position - 1
        @values[adapted_position].increase
    end

    def set_counter_to_max
        max_counter_value = to_a.max
        @values.to_a.each do |value|
            value.max_counter(max_counter_value)
        end
    end
    
    private

    def build_counters(counters_quantity)
        values = Array.new()

        counters_quantity.times do
            values.push(Counter.new)
        end
        values
    end
end