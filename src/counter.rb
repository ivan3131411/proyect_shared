class Counter
    def initialize
        @value = 0
    end

    def increase
        @value += 1
    end
    
    def max_counter(maximum_value)
        @value = maximum_value
    end

    def to_i
        @value
    end
    
end