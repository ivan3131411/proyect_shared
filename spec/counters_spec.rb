require_relative "../src/counters"

describe Counters do
    it 'given 5 counters, initially set to 0' do

        counters = Counters.new(5)

        expect(counters.to_a).to eq([0,0,0,0,0])
    end
    it 'given 3 counters, initially set to 0' do

        counters = Counters.new(3)

        expect(counters.to_a).to eq([0,0,0])
    end
    it 'increases the given counter position' do

        counters = Counters.new(3)

        counters.increase(2)

        expect(counters.to_a).to eq([0,1,0])
    end
    it 'increases the first counter position' do

        counters = Counters.new(3)

        counters.increase(1)

        expect(counters.to_a).to eq([1,0,0])
    end
    it 'set counters to max value' do

        counters = Counters.new(3)

        counters.increase(1)
        counters.increase(1)
        counters.increase(1)

        counters.set_counter_to_max

        expect(counters.to_a).to eq([3,3,3])
    end
end
